CURRENT_DIR=$(shell pwd)

APP=$(shell basename ${CURRENT_DIR})

REGISTRY=gitlab.com
TAG=latest
ENV_TAG=latest
PROJECT_NAME=${PROJECT_NAME}

IMG_NAME=${APP}
REGISTRY=${REGISTRY}


build:
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${CURRENT_DIR}/main.go

build-image:
	docker build --rm -t ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG} .
	docker tag ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG} ${REGISTRY}/${PROJECT_NAME}/${APP}:${ENV_TAG}

push-image:
	docker push ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG}
	docker push ${REGISTRY}/${PROJECT_NAME}/${APP}:${ENV_TAG}

pull-image:
	docker pull ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG}

docker-run:
	docker run --name=${BRANCH} -p 8080:8080 -itd ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG}
