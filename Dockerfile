# workspace (GOPATH) configured at /go
FROM golang:latest as builder


RUN mkdir -p $GOPATH/src/gitlab.com/gateway
WORKDIR $GOPATH/src/gitlab.com/sanjar0126/helloworld

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/helloworld /

FROM alpine
COPY --from=builder helloworld .

ENTRYPOINT ["/helloworld"]
